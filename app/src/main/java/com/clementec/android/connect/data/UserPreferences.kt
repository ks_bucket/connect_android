package com.clementec.android.connect.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*

class UserPreferences constructor(private val context: Context) {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")
    private val passcodeKey = stringPreferencesKey("passcode")
    private val localizedKey = stringPreferencesKey("localized")

    private val receiptPrinterKey = stringPreferencesKey("receiptPrinter")

    private val kyuyoEnableKey = booleanPreferencesKey("kyuyoEnable")

    private val creditEnableKey = booleanPreferencesKey("creditEnable")
    private val creditEnablePrintKey = booleanPreferencesKey("creditEnablePrint")

    private val suicaEnableKey = booleanPreferencesKey("suicaEnable")
    private val suicaEnablePrintKey = booleanPreferencesKey("suicaEnablePrint")

    private val qpEnableKey = booleanPreferencesKey("qpEnable")
    private val qpEnablePrintKey = booleanPreferencesKey("qpEnablePrint")

    private val idCreditEnableKey = booleanPreferencesKey("idCreditEnable")
    private val idCreditEnableDutyKey = booleanPreferencesKey("idCreditEnableDuty")
    private val idCreditEnableProductCodeKey = booleanPreferencesKey("idCreditEnableProductCode")
    private val idCreditEnablePrintKey = booleanPreferencesKey("idCreditEnablePrint")

    private val trainingModeKey = booleanPreferencesKey("trainingMode")

    private val uuidKey = stringPreferencesKey("uuid")

    val uuid: Flow<String> = context.dataStore.data
        .map { preferences ->
            preferences[uuidKey]?: context.dataStore.edit {
                it[uuidKey] = UUID.randomUUID().toString()
            }[uuidKey]?: ""
        }
//        .stateIn(
//            scope = CoroutineScope(Dispatchers.IO),
//            started = SharingStarted.WhileSubscribed(5000),
//            initialValue = ""
//        )

    val passcode: StateFlow<String> = context.dataStore.data
        .map { preferences ->
            preferences[passcodeKey]?: ""
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = ""
        )

    val localized: StateFlow<String> = context.dataStore.data
        .map { preferences ->
            preferences[localizedKey]?: ""
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = ""
        )

    val receiptPrinter: StateFlow<String> = context.dataStore.data
        .map { preferences ->
            preferences[receiptPrinterKey]?: ""
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = ""
        )

    val kyuyoEnable: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[kyuyoEnableKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )

    val creditEnable: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[creditEnableKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )
    val creditEnablePrint: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[creditEnablePrintKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )

    val suicaEnable: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[suicaEnableKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )
    val suicaEnablePrint: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[suicaEnablePrintKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )

    val qpEnable: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[qpEnableKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )
    val qpEnablePrint: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[qpEnablePrintKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )

    val idCreditEnable: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[idCreditEnableKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )

    val idCreditEnableDuty : StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[idCreditEnableDutyKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )
    val idCreditEnableProductCode: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[idCreditEnableProductCodeKey]?: false
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = false
        )
    val idCreditEnablePrint: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[idCreditEnablePrintKey]?: false
        }
        .stateIn(
        scope = CoroutineScope(Dispatchers.IO),
        started = SharingStarted.WhileSubscribed(5000),
        initialValue = false
        )

    val trainingMode: StateFlow<Boolean> = context.dataStore.data
        .map { preferences ->
            preferences[trainingModeKey]?: true
        }
        .stateIn(
            scope = CoroutineScope(Dispatchers.IO),
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = true
        )

    suspend fun setPasscode(value: String) {
        context.dataStore.edit {
            it[passcodeKey] = value
        }
    }

    suspend fun setLocalized(value: String) {
        context.dataStore.edit {
            it[localizedKey] = value
        }
    }

    suspend fun setReceiptPrinter(value: String) {
        context.dataStore.edit {
            it[receiptPrinterKey] = value
        }
    }

    suspend fun setKyuyoEnable(value: Boolean) {
        context.dataStore.edit {
            it[kyuyoEnableKey] = value
        }
    }

    suspend fun setCreditEnable(value: Boolean) {
        context.dataStore.edit {
            it[creditEnableKey] = value
        }
    }
    suspend fun setCreditEnablePrint(value: Boolean) {
        context.dataStore.edit {
            it[creditEnablePrintKey] = value
        }
    }

    suspend fun setSuicaEnable(value: Boolean) {
        context.dataStore.edit {
            it[suicaEnableKey] = value
        }
    }
    suspend fun setSuicaEnablePrint(value: Boolean) {
        context.dataStore.edit {
            it[suicaEnablePrintKey] = value
        }
    }

    suspend fun setQPEnable(value: Boolean) {
        context.dataStore.edit {
            it[qpEnableKey] = value
        }
    }
    suspend fun setQPEnablePrint(value: Boolean) {
        context.dataStore.edit {
            it[qpEnablePrintKey] = value
        }
    }

    suspend fun setiDEnable(value: Boolean) {
        context.dataStore.edit {
            it[idCreditEnableKey] = value
        }
    }
    suspend fun setiDEnableDuty(value: Boolean) {
        context.dataStore.edit {
            it[idCreditEnableDutyKey] = value
        }
    }
    suspend fun setiDEnableProductCode(value: Boolean) {
        context.dataStore.edit {
            it[idCreditEnableProductCodeKey] = value
        }
    }
    suspend fun setiDEnablePrint(value: Boolean) {
        context.dataStore.edit {
            it[idCreditEnablePrintKey] = value
        }
    }

    suspend fun setTrainingMode(value: Boolean) {
        context.dataStore.edit {
            it[trainingModeKey] = value
        }
    }
}
