package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class ProductData(
    @Json(name = "category_name") val categoryName: String?,
    val cd: String,
    @Json(name = "custom_data") val customData: String?,
    @Json(name = "image_file_url") val imageFileUrl: String?,
    @Json(name = "image_updated_at") val imageUpdatedAt: String?,
    val name: String,
    val price: Int,
    @Json(name = "sort_no") val sortNo: Int,
    @Json(name = "tax_rate") val taxRate: Int,
    @Json(name = "tax_type") val taxType: Int
)