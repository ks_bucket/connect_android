package com.clementec.android.connect

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.clementec.android.connect.controller.OrderHandler

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun PriceSum(orderHandler: OrderHandler) {
    val price = orderHandler.price
    val tax = orderHandler.tax

    var state = remember {
        MutableTransitionState(false).apply {
            // Start the animation immediately.
            targetState = true
        }
    }

    AnimatedVisibility(visibleState = state, enter = fadeIn(), exit = fadeOut()) {
        Column(verticalArrangement = Arrangement.Center) {
            Row(modifier = Modifier.fillMaxWidth().padding(start = 10.dp, end = 10.dp)) {
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 4.dp)
                        .wrapContentWidth(Alignment.Start),
                    text = "小計",
//                    fontSize = 18.sp,
                    color = Color.Black
                )

                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 4.dp)
                        .wrapContentWidth(Alignment.End),
                    text = "¥ $price",
//                    fontSize = 20.sp,
                    color = Color.Black
                )
            }

            Row(modifier = Modifier.fillMaxWidth().padding(start = 10.dp, end = 10.dp)) {
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 4.dp)
                        .wrapContentWidth(Alignment.Start),
                    text = "内税 10%",
//                    fontSize = 18.sp,
                    color = Color.Black
                )

                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 4.dp)
                        .wrapContentWidth(Alignment.End),
                    text = "¥ $tax",
//                    fontSize = 20.sp,
                    color = Color.Black
                )
            }

            Canvas(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp)
            ) {
                val canvasWidth = size.width
                drawLine(
                    start = Offset(x = 0f, y = 0f),
                    end = Offset(x = canvasWidth, y = 0f),
                    color = Color.Black
                )
            }

            Row(modifier = Modifier.fillMaxWidth().padding(start = 10.dp, end = 10.dp)) {
                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 4.dp)
                        .wrapContentWidth(Alignment.Start),
                    text = "合計",
//                    fontSize = 18.sp,
                    color = Color.Black
                )

                Text(
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = 4.dp)
                        .wrapContentWidth(Alignment.End),
                    text = "¥ $price",
//                    fontSize = 20.sp,
                    color = Color.Black
                )
            }
        }
    }
}
