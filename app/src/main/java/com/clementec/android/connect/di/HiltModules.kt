package com.clementec.android.connect.di

import android.content.Context
import androidx.room.Room
import com.clementec.android.connect.api.*
import com.clementec.android.connect.data.UserPreferences
import com.clementec.android.connect.data.database.Database
import com.clementec.android.connect.data.persistence.ProductDao
import com.clementec.android.connect.data.persistence.SaleDao
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton
import dagger.hilt.components.SingletonComponent
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.net.CookieHandler
import java.net.CookieManager

@Module
@InstallIn(SingletonComponent::class)
object DiModule {
    const val baseURL = "https://stg-connect.paymentcl.com"
    private const val apiPath = "/api/"
    private val cookieHandler: CookieHandler = CookieManager()
    private val interceptor: HttpLoggingInterceptor by lazy {
        HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    private val client: OkHttpClient by lazy {
        OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .cookieJar(JavaNetCookieJar(cookieHandler))
            .build()
    }

    private val moshi: Moshi by lazy {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .baseUrl(baseURL + apiPath)
            .build()
    }

    @Singleton
    @Provides
    fun client(): OkHttpClient {
        return client
    }

    @Singleton
    @Provides
    fun db(@ApplicationContext context: Context): Database {
        return Room.databaseBuilder(context, Database::class.java, "database").build()
    }

    @Singleton
    @Provides
    fun login(): LoginService {
        return retrofit.create(LoginService::class.java)
    }

    @Singleton
    @Provides
    fun productService(): ProductDataService {
        return retrofit.create(ProductDataService::class.java)
    }

    @Singleton
    @Provides
    fun sync(): SaleService {
        return retrofit.create(SaleService::class.java)
    }

    @Singleton
    @Provides
    fun pref(@ApplicationContext context: Context): UserPreferences {
        return UserPreferences(context)
    }
}