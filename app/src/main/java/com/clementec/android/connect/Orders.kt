package com.clementec.android.connect

import android.inputmethodservice.Keyboard
import androidx.compose.animation.*
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.ExpandLess
import androidx.compose.material.icons.outlined.ExpandMore
import androidx.compose.material.icons.twotone.Cancel
import androidx.compose.material.icons.twotone.HighlightOff
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.clementec.android.connect.controller.Order
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.model.ProductViewModel
import com.clementec.android.connect.data.persistence.Product

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun Orders(loginResponse: LoginResponse, viewModel: ProductViewModel) {
    val orders by viewModel.orders.collectAsState()
    val orderHandler = viewModel.orderHandler

    if (orders.isEmpty()) {
        OrderTutorial()
    } else {
        LazyColumn(modifier = Modifier.fillMaxSize().padding(10.dp)) {
            items(orders) {
                Order(it, orderHandler)
            }
        }
    }
}

@Composable
private fun Order(order: Order, orderHandler: OrderHandler) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Box {
            Row(modifier = Modifier.fillMaxWidth().background(Color.White)) {
                Image(bitmap = order.product.image!!.asImageBitmap(),
                    contentDescription = "Map snapshot",
                    modifier = Modifier.weight(2f).height(60.dp),
                    contentScale = ContentScale.Fit
                )

                Column(modifier = Modifier.weight(4f)) {
                    Text(text = order.product.name, fontSize = 12.sp, modifier = Modifier.padding(8.dp), maxLines = 1, color = Color.Black)
                    Text(text = "¥ " + order.product.price.toString(), fontSize = 12.sp, modifier = Modifier.padding(8.dp), color = Color.Black)
                }

                OrderCount(order = order, orderHandler = orderHandler)
            }

            Icon(Icons.Filled.Cancel,
                contentDescription = "Transition list",
                modifier = Modifier.size(32.dp).clickable { orderHandler.delete(order.product) })
        }
    }
}

@Composable
private fun OrderCount(order: Order, orderHandler: OrderHandler) {
    val count = order.count
    Surface(
        modifier = Modifier.padding(2.dp),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column(modifier = Modifier.background(Color(0xFF797979)),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Icon(Icons.Filled.ExpandLess,
                tint = Color.White,
                contentDescription = "Transition list",
                modifier = Modifier.size(20.dp).clickable { orderHandler.add(order.product) })

            Text(text = count.toString(),
                color = Color.White,
                fontSize = 14.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.width(20.dp))

            Icon(Icons.Filled.ExpandMore,
                tint = Color.White,
                contentDescription = "Transition list",
                modifier = Modifier.size(20.dp).clickable { orderHandler.decrease(order.product) })
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun OrderTutorial() {
    var state = remember {
        MutableTransitionState(false).apply {
            // Start the animation immediately.
            targetState = true
        }
    }

    AnimatedVisibility(visibleState = state, enter = fadeIn(), exit = fadeOut()) {
        Row(modifier = Modifier.fillMaxSize(),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically) {
            Column(modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center) {
                Column(modifier = Modifier.width(120.dp)) {
                    val painter = painterResource(id = R.drawable.tutorial)
                    Image(painter = painter,
                        contentDescription = "",
                        contentScale = ContentScale.Fit)
                }

                Spacer(modifier = Modifier.height(20.dp))

                Text(text = "商品をタッチしてください", fontSize = 14.sp)
            }
        }
    }
}