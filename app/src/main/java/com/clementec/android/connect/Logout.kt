package com.clementec.android.connect

import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.menu.Screen
import com.clementec.android.connect.data.model.LoginViewModel
import androidx.lifecycle.viewmodel.compose.viewModel

@Composable
fun Logout(viewModel: LoginViewModel) {
    viewModel.logout()
    Surface(
        Modifier.fillMaxSize(),
        color = MaterialTheme.colors.background
    ) {
        BoxWithConstraints(contentAlignment = Alignment.Center) {
            LinearProgressIndicator(
                Modifier
                    .fillMaxWidth()
                    .height(2.dp))
        }
    }
}