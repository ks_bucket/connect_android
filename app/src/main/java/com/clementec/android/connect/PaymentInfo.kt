package com.clementec.android.connect

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.menu.Screen
import com.clementec.android.connect.data.model.ProductViewModel

@Composable
fun PaymentInfo(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    val orders by viewModel.orders.collectAsState()
    val sales by viewModel.sales(loginResponse = loginResponse).collectAsState()
    for (sale in sales) {
        if (!sale.sync) {
            viewModel.register(loginResponse = loginResponse, sale = sale)
        }
    }

    Column{
        Surface(
            modifier = Modifier
                .fillMaxHeight(fraction = 0.6f)
                .fillMaxWidth()
                .padding(bottom = 10.dp),
            elevation = 2.dp,
            shape = RoundedCornerShape(8.dp)
        ) {
            FixedOrders(loginResponse = loginResponse, viewModel = viewModel)
        }

        if (orders.isNotEmpty()) {
            Surface(
                modifier = Modifier
                    .weight(2f)
                    .fillMaxSize()
                    .padding(bottom = 10.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                PriceSum(orderHandler = viewModel.orderHandler)
            }

            Surface(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxSize()
                    .padding(bottom = 10.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                OutlinedButton(
                    onClick = {
                        viewModel.orderHandler.reset()
                        navController.navigate(Screen.Menu.name) {
                            popUpTo(Screen.Menu.name) { inclusive = true } }
                              },
                    modifier = Modifier.fillMaxSize(),
                    colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color(0xFF797979), contentColor = Color.White, disabledContentColor = Color(0xFF797979)),
                ) {
                    Text("注文を中止する",
                        fontSize = 20.sp,
                        color = Color.White,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis)
                }
            }
        }
    }
}