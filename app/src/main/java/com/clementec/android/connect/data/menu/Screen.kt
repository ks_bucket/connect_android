package com.clementec.android.connect.data.menu

enum class ScreenType {
    WEB,
    MENU,
    RECEIPT,
    PAYMENTCATEGORY,
    EMONEY,
    SETTING,
    SUICABALANCE,
    PAYMENTHISTORY,
    LOGOUT
}

sealed class Screen(val name: String, val type: ScreenType, val desc: String) {
    object Menu: Screen(name = "menu", type = ScreenType.MENU, desc = "メニュー")
    object Receipt: Screen(name = "receipt", type = ScreenType.RECEIPT, desc = "レシート")
    object PaymentCategory: Screen(name = "paymentCategory", type = ScreenType.PAYMENTCATEGORY, desc = "支払い方法")
    object Emoney: Screen(name = "emoney", type = ScreenType.EMONEY, desc = "電子マネー")
    object Setting: Screen(name = "setting", type = ScreenType.SETTING, desc = "設定")
    object SuicaBalance: Screen(name = "suicaBalance", type = ScreenType.SUICABALANCE, desc = "suica残高参照")
    object PaymentHistory: Screen(name = "paymentHistory", type = ScreenType.PAYMENTHISTORY, desc = "決済履歴")
    object Web: Screen(name = "web", type = ScreenType.WEB, desc = "ホームページ")
    object Logout: Screen(name = "logout", type = ScreenType.LOGOUT, desc = "ログアウト")
}