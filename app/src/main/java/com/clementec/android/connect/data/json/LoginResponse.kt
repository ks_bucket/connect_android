package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class LoginResponse(
    @Json(name = "company_id") val companyId: Int,
    @Json(name = "company_name") val companyName: String,
    @Json(name = "err_msg") val errMsg: String,
    @Json(name = "err_no") val errNo: String,
    val receipt: Receipt,
    val result: Int,
    @Json(name = "session_id") val sessionId: String,
    @Json(name = "use_service_id") val useServiceId: Int,
    @Json(name = "use_service_name") val useServiceName: String,
    @Json(name = "use_service_name_type") val useServiceNameType: Int,
    @Json(name = "user_id") val userId: Int
)
