package com.clementec.android.connect.api

import android.os.Parcelable
import com.clementec.android.connect.data.json.ProductDataSource
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ProductDataService {
    @Headers("Content-Type: application/json", "Accept: application/json", "Cache-Control: no-cache")
    @POST("get-screen/")
    suspend fun load(@Body data: ProductRequestData) : ProductDataSource
}

data class ProductRequestData(
    val session_id: String,
    val use_service_id: Int,
    val user_id: Int
)