package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class Title(
    @Json(name = "image_updated_at") val imageUpdatedAt: String,
    @Json(name = "image_url") val imageUrl: String
)