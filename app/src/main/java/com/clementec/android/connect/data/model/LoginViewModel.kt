package com.clementec.android.connect.data.model

import android.util.Log
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clementec.android.connect.api.ApiState
import com.clementec.android.connect.data.UserPreferences
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.repository.LoginRepository
import com.clementec.android.connect.data.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(private val repository: LoginRepository, val preferences: UserPreferences): ViewModel() {
    private val _state: MutableStateFlow<ApiState> = MutableStateFlow(ApiState.Idle)
    val state: StateFlow<ApiState> = _state

    var response by mutableStateOf<LoginResponse?>( null )

    fun login(id: String, pwd: String) {
        viewModelScope.launch {
            _state.value = ApiState.Loading
            delay(3000)

            val res = repository.login(id, pwd)
            response = res
            if (res != null) {
                _state.value = if (res.result == 0) {
                    ApiState.Done
                } else {
                    ApiState.Error
                }
            } else {
                _state.value = ApiState.Error
            }
        }
    }

    fun reset() {
        viewModelScope.launch {
            response = null
            _state.value = ApiState.Idle
        }
    }

    fun logout() {
        viewModelScope.launch {
            delay(3000)
            reset()
        }
    }
}