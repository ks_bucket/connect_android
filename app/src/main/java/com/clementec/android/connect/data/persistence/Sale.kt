package com.clementec.android.connect.data.persistence

import androidx.annotation.NonNull
import androidx.room.*
import com.clementec.android.connect.api.SaleOrderInfo
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
interface SaleDao {
    @Query("SELECT * FROM sale")
    fun allSales(): Flow<List<Sale>>

    @Query("SELECT * FROM sale WHERE use_service_id == :serviceId AND company_id == :companyId ORDER BY sale_id ASC")
    fun sales(serviceId: Int, companyId: Int): Flow<List<Sale>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(sale: Sale)

    @Update
    suspend fun update(sale: Sale)

    @Delete
    suspend fun delete(sale: Sale)
}

@Entity
data class Sale(
    @PrimaryKey val sale_id: Long = Date().time,
    // 利用サービスID
    @NonNull @ColumnInfo(name = "use_service_id") val useServiceId: Int,
    // 会社情報
    @NonNull @ColumnInfo(name = "company_id") val companyId: Int,
    // 端末アカウント
    @NonNull @ColumnInfo(name = "user_id") val userId: Int,
    // QRコード電文
    @NonNull @ColumnInfo(name = "qr_cd") val qrCd: String,
    // 売上情報
    @NonNull @ColumnInfo(name = "order_info") val orderInfo: String,
    // 売上同期「登録済か」
    var sync: Boolean,
    // 決済アプリ処理結果
    val payment: String,
    // レシート印字済
    val printed: Boolean
)