package com.clementec.android.connect.api

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import coil.ImageLoader
import coil.request.ImageRequest
import coil.request.SuccessResult
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import okhttp3.OkHttpClient
import javax.inject.Inject

class Loader @Inject constructor(@ApplicationContext private val context: Context,
                                 private val client: OkHttpClient) {
    private val imageLoader = ImageLoader.Builder(context).okHttpClient(client).build()

    suspend fun image(url: String): Bitmap? = coroutineScope {
        val deferred = async(Dispatchers.IO) {
            try {
                val request = ImageRequest.Builder(context)
                    .data(url)
                    .build()

                val result = imageLoader.execute(request) as? SuccessResult
                val bitmapDrawable = result?.drawable as? BitmapDrawable
                bitmapDrawable?.bitmap
            } catch(ex: Throwable) {
                Log.d("Login", "$ex")
                null
            }
        }

        deferred.await()
    }
}