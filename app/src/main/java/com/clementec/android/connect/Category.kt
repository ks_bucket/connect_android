package com.clementec.android.connect

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.graphics.Color
import androidx.compose.material.*
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.material.ButtonDefaults

@Composable
fun Category(name: String, selected: String, onClick: () -> Unit) {
    val bgColor = if (name == selected) Color(0xFF797979) else Color(0xfff0f0f0)
    OutlinedButton(
        onClick = { onClick.invoke() },
        modifier = Modifier.height(40.dp).width(140.dp).padding(start = 10.dp),
        colors = ButtonDefaults.outlinedButtonColors(backgroundColor = bgColor, contentColor = Color.Black, disabledContentColor = Color.Gray),
    ) {
        Text(name, color = Color.Black, maxLines = 1, overflow = TextOverflow.Ellipsis)
    }
}