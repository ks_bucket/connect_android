package com.clementec.android.connect.data.repository

import android.util.Log
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.lifecycle.viewModelScope
import com.clementec.android.connect.Util.toLocalDatetime
import com.clementec.android.connect.api.*
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.database.Database
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.persistence.Product
import com.clementec.android.connect.data.persistence.Sale
import com.clementec.android.connect.data.persistence.SaleDao
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.adapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.util.*
import javax.inject.Inject
import kotlin.math.log
import kotlin.random.Random.Default.nextLong

class SaleRepository @Inject constructor(private val api: SaleService, private val db: Database) {
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)
    private val saleDao = db.saleDao()
    private val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    @OptIn(ExperimentalStdlibApi::class)
    private val saleInfoAdapter = moshi.adapter<SaleOrderInfo>()
    @OptIn(ExperimentalStdlibApi::class)
    private val saleInfoListAdapter = moshi.adapter<List<SaleOrderInfo>>()
    private var insertedSale: Flow<List<Sale>>? = null

    fun sales(loginResponse: LoginResponse): Flow<List<Sale>> {
        val type = loginResponse.useServiceNameType
        val id = loginResponse.companyId
        return saleDao.sales(type, id)
    }

    private fun stringToOrderInfo(jsonString: String): SaleOrderInfo? {
        return saleInfoAdapter.fromJson(jsonString)
    }

    private fun orderInfoToString(info: SaleOrderInfo): String {
        return saleInfoAdapter.toJson(info)
    }

    private fun stringToOrderInfoList(value: String): List<SaleOrderInfo>? {
        return saleInfoListAdapter.fromJson(value)
    }

    private fun orderInfoListToString(info: List<SaleOrderInfo>): String? {
        return saleInfoListAdapter.toJson(info)
    }

    private fun orderDetail(orderHandler: OrderHandler): List<OrderDetail> {
        return orderHandler.list.value.map { OrderDetail(product_cd = it.product.cd,
            product_name = it.product.name,
            product_quantity = it.count,
            product_price = it.product.price,
            product_tax_type = it.product.taxType,
            product_tax_rate = it.product.taxRate) }
    }

    private fun orderInfo(uuid: String, hostType: String, slipNumber: String, recordType: Record, orderHandler: OrderHandler): List<SaleOrderInfo> {
        val detail = orderDetail(orderHandler = orderHandler)
        return listOf(SaleOrderInfo(terminal_udid = uuid,
            order_date = Date().toLocalDatetime().toString(),
            order_host_type = hostType,
            slip_number = slipNumber,
            record_type = recordType.value,
            total_price = orderHandler.price,
            consumption_tax = orderHandler.tax,
            order_detail = detail))
    }

    suspend fun save(loginResponse: LoginResponse, qr: String = "", uuid: String, hostType: String, slipNumber: String, recordType: Record, orderHandler: OrderHandler, payment: String) {
        scope.launch(Dispatchers.IO) {
            val orderInfo = orderInfo(uuid = uuid,
                hostType = hostType,
                slipNumber = slipNumber,
                recordType = recordType,
                orderHandler = orderHandler)
            orderInfoListToString(info = orderInfo)?.let {
                val sale = Sale(useServiceId = loginResponse.useServiceNameType,
                    companyId = loginResponse.companyId,
                    userId = loginResponse.userId,
                    qrCd = qr,
                    orderInfo = it,
                    payment = payment,
                    sync = false,
                    printed = false)

                saleDao.insert(sale = sale)
                Log.d("insert sale:", "$sale")
            }
        }
    }

    private fun data(response: LoginResponse, sale: Sale): SaleData? {
        val sessionId = response.sessionId
        val serviceId = response.useServiceId
        val userId = response.userId

        return stringToOrderInfoList(value = sale.orderInfo)?.let {
            SaleData(session_id = sessionId, use_service_id = serviceId, user_id = userId, qr_cd = "", order_info = it)
        } ?: null
    }

    suspend fun register(loginResponse: LoginResponse, sale: Sale) {
        scope.launch(Dispatchers.IO) {
            try {
                data(loginResponse, sale)?.let {
                    val res = api.sync(it)
                    if (res.result == 0) {
                        var tmpSale = sale.copy(sync = true)
                        Log.d("tmp sale:", "$tmpSale")
                        saleDao.update(tmpSale)
                    }
                    Log.d("register sale:", "$sale")
                }
            } catch (ex: Throwable) {
                Log.d("load product exception:", "${(ex as? HttpException)?.response()?.errorBody()?.string()}")
            }
        }
    }
}