package com.clementec.android.connect.data.repository

import android.graphics.Bitmap
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.viewModelScope
import com.clementec.android.connect.ConnectApplication
import com.clementec.android.connect.Util.toLocalDatetime
import com.clementec.android.connect.api.Api
import com.clementec.android.connect.api.Loader
import com.clementec.android.connect.api.ProductDataService
import com.clementec.android.connect.api.ProductRequestData
import com.clementec.android.connect.data.DataProvider
import com.clementec.android.connect.data.database.Database
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.json.ProductData
import com.clementec.android.connect.data.persistence.Product
import com.clementec.android.connect.data.persistence.ProductDao
import com.clementec.android.connect.data.persistence.ProductMinimal
import com.clementec.android.connect.di.DiModule
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.android.scopes.ViewScoped
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.HttpException
import java.security.MessageDigest
import java.util.*
import javax.inject.Inject
import kotlin.math.log

class ProductRepository @Inject constructor(private val api: ProductDataService,
                                            private val loader: Loader,
                                            private val db: Database) {

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)
    private val productDao = db.productDao()

    fun distinctProduct(cd: String, loginResponse: LoginResponse): Flow<Product> {
        val type = loginResponse.useServiceNameType
        val id = loginResponse.companyId
        return productDao.distinctProduct(cd, type, id)
    }

    fun productMinimals(loginResponse: LoginResponse): Flow<List<ProductMinimal>> {
        val type = loginResponse.useServiceNameType
        val id = loginResponse.companyId
        return productDao.productMinimals(type, id)
    }

    fun products(loginResponse: LoginResponse): Flow<List<Product>> {
        val type = loginResponse.useServiceNameType
        val id = loginResponse.companyId
        return productDao.getAll(type, id)
    }

    private fun request(response: LoginResponse): ProductRequestData {
        val sessionId = response.sessionId
        val serviceId = response.useServiceId
        val userId = response.userId
        return ProductRequestData(sessionId, serviceId, userId)
    }

    suspend fun load(loginResponse: LoginResponse) {
        scope.launch(Dispatchers.IO) {
            try {
                val data = request(loginResponse)
                val res = api.load(data)
                Log.d("load product exception:", "$res")
                res.product?.let { it ->
                    it.asFlow()
                    .onEach { data ->
                        save(product = data, loginResponse = loginResponse)
                    }
                    .collect()
                }
            } catch (ex: Throwable) {
                Log.d("load product exception:", "${(ex as? HttpException)?.response()?.errorBody()?.string()}")
            }
        }
    }

    suspend fun update(product: Product) {
        scope.launch(Dispatchers.IO) {
            productDao.update(product)
        }
    }

    suspend fun save(product: Product) {
        scope.launch(Dispatchers.IO) {
            productDao.insert(product)
        }
    }

    suspend fun save(product: ProductData, loginResponse: LoginResponse) {
        scope.launch(Dispatchers.IO) {
            fetchImageOf(product)?.let {
                val entity = entityFrom(product = product, image = it, loginResponse = loginResponse)
                save(entity)
            }
        }
    }

    private suspend fun fetchImageOf(product: ProductData, baseUrl: String = DiModule.baseURL): Bitmap? {
        val bitmap = product.imageFileUrl?.let { imagePath ->
            loader.image(baseUrl + imagePath)
        }

        return bitmap
    }

    private fun entityFrom(product: ProductData, image: Bitmap, loginResponse: LoginResponse): Product {
        val sortNo: Int = product.sortNo
        val serviceId: Int = loginResponse.useServiceNameType
        val companyId: Int = loginResponse.companyId
        val id: Int = (companyId.toString() + serviceId.toString() + sortNo).toInt()

        return Product(id = id,
            cd = product.cd,
            name = product.name,
            price = product.price,
            taxType = product.taxType,
            taxRate = product.taxRate,
            categoryName = product.categoryName,
            imageFileUrl = product.imageFileUrl,
            imageUpdatedAt = product.imageUpdatedAt,
            sortNo = sortNo,
            customData = product.customData,
            serviceId = serviceId,
            companyId = companyId,
            date = Date().toLocalDatetime().toString(),
            image = image
        )
    }
}