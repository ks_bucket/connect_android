package com.clementec.android.connect

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.Menu
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.clementec.android.connect.controller.Order
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.menu.*
import com.clementec.android.connect.data.model.LoginViewModel
import com.clementec.android.connect.data.persistence.Sale

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun PaymentHistory(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    val sales by viewModel.sales(loginResponse = loginResponse).collectAsState()
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xf0f0f0))
            .padding(start = 10.dp, end = 10.dp)
    ) {
        Scaffold(
            Modifier.weight(2.4f),
            topBar = { TopBar(navController = navController, title = "決済履歴")
            }
        ) {
            LazyColumn(modifier = Modifier.fillMaxSize().padding(10.dp)) {
                items(sales) {
                    Sale(it as Sale)
                }
            }
        }
    }
}

@Composable
private fun Sale(sale: Sale) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Box {
            Row(modifier = Modifier.fillMaxWidth().background(Color.White)) {
                Column(modifier = Modifier.weight(4f)) {
                    Text(text = sale.sale_id.toString(), fontSize = 12.sp, modifier = Modifier.padding(8.dp), maxLines = 1, color = Color.Black)
                    Text(text = sale.orderInfo.toString(), fontSize = 12.sp, modifier = Modifier.padding(8.dp), color = Color.Black)
                }
            }
        }
    }
}