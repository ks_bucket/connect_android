package com.clementec.android.connect.data.model

import android.util.Log
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.persistence.Product
import com.clementec.android.connect.data.repository.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.WhileSubscribed
import javax.inject.Inject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.clementec.android.connect.Util.toLocalDatetime
import com.clementec.android.connect.api.Record
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.UserPreferences
import com.clementec.android.connect.data.persistence.ProductMinimal
import com.clementec.android.connect.data.persistence.Sale
import com.clementec.android.connect.data.repository.SaleRepository
import kotlinx.coroutines.launch
import java.util.*

@HiltViewModel
class ProductViewModel @Inject constructor(private val repository: ProductRepository,
                                           private val saleRepository: SaleRepository,
                                           val orderHandler: OrderHandler,
                                           val preferences: UserPreferences
): ViewModel() {
    private lateinit var uuid: String
    val orders = orderHandler.list

    init {
        viewModelScope.launch {
            preferences.uuid.collect {
                uuid = it
            }
        }
    }

    fun order(loginResponse: LoginResponse, qr: String, hostType: String, slipNumber: String, recordType: Record, payment: String) {
        viewModelScope.launch {
            saleRepository.save(loginResponse = loginResponse,
                qr = qr,
                uuid = uuid,
                hostType = hostType,
                slipNumber = slipNumber,
                recordType = recordType,
                orderHandler = orderHandler,
                payment = payment)
        }
    }

    fun sales(loginResponse: LoginResponse) = saleRepository.sales(loginResponse = loginResponse)
        .stateIn(
            initialValue = emptyList<Sale>(),
            scope = viewModelScope,
            started = WhileSubscribed(5000)
        )

    fun register(loginResponse: LoginResponse, sale: Sale) {
        viewModelScope.launch {
            saleRepository.register(loginResponse = loginResponse, sale = sale)
        }
    }

    fun distinctProduct(cd: String, loginResponse: LoginResponse) = repository.distinctProduct(cd = cd, loginResponse = loginResponse)
        .stateIn(
            initialValue = null,
            scope = viewModelScope,
            started = WhileSubscribed(5000)
        )

    fun productMinimals(loginResponse: LoginResponse) = repository.productMinimals(loginResponse = loginResponse)
        .stateIn(
            initialValue = emptyList<ProductMinimal>(),
            scope = viewModelScope,
            started = WhileSubscribed(5000)
        )

    fun products(loginResponse: LoginResponse) = repository.products(loginResponse = loginResponse)
        .stateIn(
            initialValue = emptyList<Product>(),
            scope = viewModelScope,
            started = WhileSubscribed(5000)
        )

    fun loadRemoteData(loginResponse: LoginResponse) {
        viewModelScope.launch {
            repository.load(loginResponse = loginResponse)
        }
    }

    fun save(product: Product) {
        viewModelScope.launch {
            repository.save(product = product)
        }
    }

    fun update(product: Product) {
        viewModelScope.launch {
            repository.update(product = product)
        }
    }
}