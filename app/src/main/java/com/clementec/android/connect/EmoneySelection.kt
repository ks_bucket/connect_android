package com.clementec.android.connect

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.clementec.android.connect.data.menu.*
import androidx.lifecycle.viewmodel.compose.viewModel
import com.clementec.android.connect.api.*
import com.clementec.android.connect.data.json.LoginResponse
import java.util.*

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun EmoneySelection(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .background(Color(0xfff0f0f0))
        .padding(start = 10.dp, end = 10.dp)
    ) {
        Scaffold(
            modifier = Modifier.weight(4f),
            topBar = { TopBar(navController = navController, title = "電子マネーの種類を選択してください") },
            backgroundColor = Color.Transparent
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 10.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                EmoneyList(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
            }
        }

        Spacer(modifier = Modifier.width(10.dp))

        Scaffold(
            Modifier.weight(1f),
            topBar = { TopAppBar(title = { Text("ご注文リスト") }, modifier = Modifier.padding(0.dp).height(40.dp), backgroundColor = Color.Transparent, elevation = 0.dp) },
            backgroundColor = Color.Transparent
        ) {
            PaymentInfo(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }
    }
}

@Composable
fun EmoneyList(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(modifier = Modifier.fillMaxSize(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically) {
        Column(modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Row(horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically) {
                TransIC(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
                Spacer(modifier = Modifier.width(50.dp))
                QuicPay(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
                Spacer(modifier = Modifier.width(50.dp))
                IDCard(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
            }
        }
    }
}

@Composable
fun TransIC(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Column(modifier = Modifier.clickable {
        viewModel.order(loginResponse = loginResponse,
            qr = "",
            hostType = Service(type = Service.Type.Suica).value,
            slipNumber = UUID.randomUUID().toString(),
            recordType = Record(type = Record.Type.SALE),
            payment = "")
    }, horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(150.dp)) {
            val painter = painterResource(id = R.drawable.trans_ic)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(32.dp))

        Text(text = "交通系IC", fontSize = 18.sp)
    }
}

@Composable
fun QuicPay(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Column(modifier = Modifier.clickable {
        viewModel.order(loginResponse = loginResponse,
            qr = "",
            hostType = Service(type = Service.Type.QP).value,
            slipNumber = UUID.randomUUID().toString(),
            recordType = Record(type = Record.Type.SALE),
            payment = "")
    },
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(150.dp)) {
            val painter = painterResource(id = R.drawable.qp_card)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(32.dp))

        Text(text = "QUICPay", fontSize = 18.sp)
    }
}

@Composable
fun IDCard(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Column(modifier = Modifier.clickable {
                                         viewModel.order(loginResponse = loginResponse,
                                             qr = "",
                                             hostType = Service(type = Service.Type.ID).value,
                                             slipNumber = UUID.randomUUID().toString(),
                                             recordType = Record(type = Record.Type.SALE),
                                             payment = "")
    },
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(150.dp)) {
            val painter = painterResource(id = R.drawable.id)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(32.dp))

        Text(text = "iD決済", fontSize = 18.sp)
    }
}
