package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class Receipt(
    val address: String,
    @Json(name = "branch_name") val branchName: String,
    @Json(name = "postal_code") val postalCode: String,
    val remarks: String
)