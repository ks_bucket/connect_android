package com.clementec.android.connect

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.api.ApiState
import com.clementec.android.connect.data.database.Database
import com.clementec.android.connect.data.model.LoginViewModel
import com.clementec.android.connect.data.model.ProductViewModel
import com.clementec.android.connect.data.repository.ProductRepository
import com.clementec.android.connect.ui.theme.ConnectTheme
import dagger.hilt.android.AndroidEntryPoint
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.ProvideWindowInsets

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalFoundationApi::class,
        androidx.compose.material.ExperimentalMaterialApi::class
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ConnectTheme {
                ProvideWindowInsets(windowInsetsAnimationsEnabled = true) {
                    Surface(
                        Modifier.fillMaxSize(),
                        color = MaterialTheme.colors.background
                    ) {
                        TopView()
                    }
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun TopView(viewModel: LoginViewModel = viewModel(), productViewModel: ProductViewModel = viewModel()) {
    val state by viewModel.state.collectAsState()
    val response = viewModel.response

    if (state == ApiState.Done && response != null) {
        NavGraph(loginResponse = response, login = viewModel, viewModel = productViewModel)
    } else {
        Login(viewModel = viewModel)
    }
}

@Composable
fun Login(viewModel: LoginViewModel) {
    val state by viewModel.state.collectAsState()

    when (state) {
        ApiState.Idle -> InputIdPwd(viewModel = viewModel)
        ApiState.Loading -> LoadingIndicator()
        ApiState.Done -> Snackbar() { Text(text = "success") }
        ApiState.Error -> ErrorDialog(viewModel = viewModel)
    }
}

@Composable
fun InputIdPwd(viewModel: LoginViewModel) {
    val insets = LocalWindowInsets.current
    val imeBottom = with(LocalDensity.current) { insets.ime.bottom.toDp() }

    Column(verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally) {
        Column(verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.End) {
            var id by remember { mutableStateOf("") }
            var pwd by remember { mutableStateOf("") }
            var enabled by remember { mutableStateOf(pwd.length >= 8) }

            OutlinedTextField(value = id, onValueChange = { id = it }, label = { Text("ID") })
            OutlinedTextField(value = pwd,
                onValueChange = { pwd = it
                                enabled = it.length >= 8
                                },
                label = { Text("パスワード") },
                visualTransformation = PasswordVisualTransformation(),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password)
            )

            Spacer(Modifier.height(8.dp))

            Button(onClick = {
                viewModel.login(id, pwd)
            }, modifier = Modifier.height(50.dp).width(150.dp).padding(bottom = imeBottom), enabled = enabled) {
                Text("ログイン")
            }
        }
    }
}

@Composable
fun LoadingIndicator() {
    BoxWithConstraints(contentAlignment = Alignment.Center) {
        LinearProgressIndicator(
            Modifier
                .fillMaxWidth()
                .height(2.dp))
    }
}

@Composable
fun ErrorDialog(viewModel: LoginViewModel = viewModel()) {
    AlertDialog(
        onDismissRequest = {
            viewModel.reset()
        },
        title = {
            Text(text = "失敗")
        },
        text = {
            Text("ログイン失敗しました。")
        },
        confirmButton = {
            Button(
                modifier = Modifier.width(100.dp),
                onClick = { viewModel.reset() }
            ) {
                Text("閉じる")
            }
        },
        modifier = Modifier
            .width(300.dp)
            .height(150.dp)
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ConnectTheme {
//        Greeting("Android")
    }
}