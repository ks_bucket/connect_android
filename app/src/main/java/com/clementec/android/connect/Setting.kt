package com.clementec.android.connect

import android.util.Log
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.SemanticsProperties.ToggleableState
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.clementec.android.connect.controller.Order
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.UserPreferences
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.menu.*
import com.clementec.android.connect.data.persistence.Product
import com.clementec.android.connect.data.persistence.ProductDao
import com.clementec.android.connect.data.persistence.ProductMinimal
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.intellij.lang.annotations.Language

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Setting(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xfff0f0f0))
            .padding(start = 10.dp, end = 10.dp)
    ) {
        Scaffold(
            topBar = { TopBar(navController = navController, title = "設定") },
            backgroundColor = Color.Transparent
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 10.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                Column(modifier = Modifier
                    .fillMaxSize()
                    .padding(10.dp)
                    .verticalScroll(rememberScrollState())) {
                    // パスコード設定
                    Text(text = "認証", fontSize = 20.sp, modifier = Modifier.padding(start = 10.dp))
                    Passcode(preferences = viewModel.preferences)

                    // 言語
                    Text(text = "言語", fontSize = 20.sp, modifier = Modifier.padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 0.dp))
                    Language(preferences = viewModel.preferences)

                    // 商品設定
                    Row(horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 0.dp)
                    ) {
                        Text(text = "商品情報", fontSize = 20.sp)
                        Icon(Icons.Filled.CloudDownload, contentDescription = "Passcode", modifier = Modifier
                            .size(32.dp)
                            .clickable { viewModel.loadRemoteData(loginResponse = loginResponse) })
                    }
                    DownloadedProducts(loginResponse = loginResponse, viewModel = viewModel)

                    // 店舗住所
                    Text(text = "レシート情報", fontSize = 20.sp, modifier = Modifier.padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 0.dp))
                    Address(loginResponse = loginResponse)

                    // 店舗住所
                    Text(text = "プリンター", fontSize = 20.sp, modifier = Modifier.padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 0.dp))
                    Printers(preferences = viewModel.preferences)

                    // 決済情報
                    Text(text = "決済方法", fontSize = 20.sp, modifier = Modifier.padding(start = 10.dp, end = 10.dp, top = 10.dp, bottom = 0.dp))
                    PaymentInfo(preferences = viewModel.preferences)
                }
            }
        }
    }
}

@Composable
private fun Passcode(preferences: UserPreferences) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .background(Color.White),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically) {
            Icon(Icons.Filled.VpnKey, contentDescription = "Passcode", modifier = Modifier
                .padding(10.dp)
                .size(32.dp))

            val passcode by preferences.passcode.collectAsState()
            OutlinedTextField(value = passcode,
                onValueChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setPasscode(value = it) } },
                modifier = Modifier
                    .width(200.dp)
                    .padding(bottom = 8.dp),
                label = { Text("パスコード") })
        }
    }
}

@Composable
private fun Language(preferences: UserPreferences) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .background(Color.White),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically) {
            Icon(Icons.Filled.Translate, contentDescription = "Language", modifier = Modifier
                .padding(10.dp)
                .size(32.dp))

            val language by preferences.localized.collectAsState()
            OutlinedTextField(value = language,
                onValueChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setLocalized(value = it) } },
                modifier = Modifier
                    .width(200.dp)
                    .padding(bottom = 8.dp),
                label = { Text("使用言語") })

            var expanded by rememberSaveable { mutableStateOf(false) }
            IconButton(onClick = { expanded = true }) {
                Icon(Icons.Filled.FactCheck, contentDescription = "Languages", modifier = Modifier
                    .padding(10.dp)
                    .size(32.dp))
            }

            Row {
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    DropdownMenuItem(onClick = {
                        expanded = false
                        CoroutineScope(Dispatchers.IO).launch { preferences.setLocalized(value = "日本語") }
                    }) {
                        Text("日本語")
                    }
                    Divider()
                    DropdownMenuItem(onClick = {
                        expanded = false
                        CoroutineScope(Dispatchers.IO).launch { preferences.setLocalized(value = "English") }
                    }) {
                        Text("English")
                    }
                }
            }
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun DownloadedProducts(loginResponse: LoginResponse, viewModel: ProductViewModel) {
    val products by viewModel.productMinimals(loginResponse = loginResponse).collectAsState()
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        LazyVerticalGrid(
            modifier = Modifier
                .fillMaxSize()
                .height(300.dp),
            cells = GridCells.Fixed(2),
            contentPadding = PaddingValues(8.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp),
            horizontalArrangement = Arrangement.spacedBy(4.dp)) {
            items(products) { product ->
                DownloadedProduct(loginResponse = loginResponse, viewModel = viewModel, product)
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun DownloadedProduct(loginResponse: LoginResponse, viewModel: ProductViewModel, product: ProductMinimal) {
    var state = remember {
        MutableTransitionState(false).apply {
            // Start the animation immediately.
            targetState = true
        }
    }

    val cd = product.cd
    val target by viewModel.distinctProduct(cd = cd, loginResponse = loginResponse).collectAsState()

    AnimatedVisibility(visibleState = state, enter = fadeIn(), exit = fadeOut()) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .background(Color.White)
            .padding(bottom = 8.dp),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically) {
            val checkedState = product.display
            Checkbox(checked = checkedState, onCheckedChange = { check ->
                target?.let {
                    it.display = check
                    CoroutineScope(Dispatchers.IO).launch {
                        viewModel.update(product = it)
                    }
                }})
            Text(text = "商品ID:", modifier = Modifier.weight(1f))
            Text(text = product.cd, maxLines = 1, modifier = Modifier.weight(2f))

            val name = product.name
            OutlinedTextField(
                value = name,
                onValueChange = { changed ->
                    target?.let {
                        CoroutineScope(Dispatchers.IO).launch {
                            it.name = changed
                            viewModel.update(product = it)
                        }
                    }
                },
                modifier = Modifier.weight(4f),
                label = { Text("商品名") }
            )

            val price = product.price.toString()
            OutlinedTextField(
                value = "¥ $price",
                onValueChange = { changed ->
                    target?.let {
                        CoroutineScope(Dispatchers.IO).launch {
                            it.price = changed.replaceFirst(oldValue = "¥ ", newValue = "", ignoreCase = true).toInt()
                            viewModel.update(product = it)
                        }
                    } },
                modifier = Modifier.weight(2f),
                label = { Text("価格") }
            )
        }
    }
}

@Composable
private fun Address(loginResponse: LoginResponse) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Column {
            Row(modifier = Modifier
                .fillMaxWidth()
                .background(Color.White),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically) {
                Icon(Icons.Filled.LocalPostOffice, contentDescription = "Passcode", modifier = Modifier
                    .padding(10.dp)
                    .size(32.dp))

                var postal by remember { mutableStateOf(loginResponse.receipt.postalCode) }
                OutlinedTextField(value = postal,
                    onValueChange = { postal = it },
                    modifier = Modifier
                        .width(200.dp)
                        .padding(bottom = 8.dp),
                    readOnly = true,
                    label = { Text("〒") })
            }

            Row(modifier = Modifier
                .fillMaxWidth()
                .background(Color.White),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically) {
                Icon(Icons.Filled.Home, contentDescription = "Passcode", modifier = Modifier
                    .padding(10.dp)
                    .size(32.dp))

                var add by remember { mutableStateOf(loginResponse.receipt.address) }
                OutlinedTextField(value = add,
                    onValueChange = { add = it },
                    modifier = Modifier
                        .width(200.dp)
                        .padding(bottom = 8.dp),
                    readOnly = true,
                    label = { Text("住所") })
            }

            Row(modifier = Modifier
                .fillMaxWidth()
                .background(Color.White),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically) {
                Icon(Icons.Filled.Store, contentDescription = "Passcode", modifier = Modifier
                    .padding(10.dp)
                    .size(32.dp))

                var branch by remember { mutableStateOf(loginResponse.receipt.branchName) }
                OutlinedTextField(value = branch,
                    onValueChange = { branch = it },
                    modifier = Modifier
                        .width(200.dp)
                        .padding(bottom = 8.dp),
                    readOnly = true,
                    label = { Text("店舗") })
            }

            Row(modifier = Modifier
                .fillMaxWidth()
                .background(Color.White),
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically) {
                Icon(Icons.Filled.Comment, contentDescription = "Passcode", modifier = Modifier
                    .padding(10.dp)
                    .size(32.dp))

                var remarks by remember { mutableStateOf(loginResponse.receipt.remarks) }
                OutlinedTextField(value = remarks,
                    onValueChange = { remarks = it },
                    modifier = Modifier
                        .width(200.dp)
                        .padding(bottom = 8.dp),
                    readOnly = true,
                    label = { Text("備考") })
            }
        }

    }
}

@Composable
private fun Printers(preferences: UserPreferences) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .background(Color.White),
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically) {
            Icon(Icons.Filled.Print, contentDescription = "Printer", modifier = Modifier
                .padding(10.dp)
                .size(32.dp))

            val printer by preferences.receiptPrinter.collectAsState()
            OutlinedTextField(value = printer,
                onValueChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setReceiptPrinter(value = it) } },
                modifier = Modifier
                    .width(200.dp)
                    .padding(bottom = 8.dp),
                label = { Text("レシート用") })

            var expanded by rememberSaveable { mutableStateOf(false) }
            IconButton(onClick = { expanded = true }) {
                Icon(Icons.Filled.FactCheck, contentDescription = "Printer list", modifier = Modifier
                    .padding(10.dp)
                    .size(32.dp))
            }

            Row {
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    DropdownMenuItem(onClick = {
                        expanded = false
                        CoroutineScope(Dispatchers.IO).launch { preferences.setReceiptPrinter(value = "PRNT Star") }
                    }) {
                        Text("PRNT Star")
                    }
                    Divider()
                    DropdownMenuItem(onClick = {
                        expanded = false
                        CoroutineScope(Dispatchers.IO).launch { preferences.setReceiptPrinter(value = "PRNT SII") }
                    }) {
                        Text("PRNT SII")
                    }
                    Divider()
                    DropdownMenuItem(onClick = {
                        expanded = false
                        CoroutineScope(Dispatchers.IO).launch { preferences.setReceiptPrinter(value = "PRNT ESP") }
                    }) {
                        Text("PRNT ESP")
                    }
                }
            }
        }
    }
}

@Composable
private fun PaymentInfo(preferences: UserPreferences) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Column {
            KyuyoInfo(preferences = preferences)
            Divider(modifier = Modifier.padding(start = 10.dp))
            CreditInfo(preferences = preferences)
            Divider(modifier = Modifier.padding(start = 10.dp))
            SuicaInfo(preferences = preferences)
            Divider(modifier = Modifier.padding(start = 10.dp))
            QuicPayInfo(preferences = preferences)
            Divider(modifier = Modifier.padding(start = 10.dp))
            IDInfo(preferences = preferences)
            Divider(modifier = Modifier.padding(start = 10.dp))
            TrainingMode(preferences = preferences)
        }
    }
}

@Composable
private fun KyuyoInfo(preferences: UserPreferences) {
    Column {
        Row(horizontalArrangement = Arrangement.Start, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)) {
            Icon(Icons.Filled.QrCodeScanner, contentDescription = "Printer list", modifier = Modifier
                .size(32.dp))
            Text(text = "給与天引き", fontWeight = FontWeight.Bold, modifier = Modifier.padding(start = 10.dp))
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "有効")

            val enabled by preferences.kyuyoEnable.collectAsState()
            Switch(checked = enabled, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setKyuyoEnable(value = it) } })
        }
    }
}

@Composable
private fun CreditInfo(preferences: UserPreferences) {
    Column {
        Row(horizontalArrangement = Arrangement.Start, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)) {
            Icon(Icons.Filled.CreditCard, contentDescription = "Printer list", modifier = Modifier
                .size(32.dp))
            Text(text = "クレジットカード", fontWeight = FontWeight.Bold, modifier = Modifier.padding(start = 10.dp))
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "有効")

            val enabled by preferences.creditEnable.collectAsState()
            Switch(checked = enabled, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setCreditEnable(value = it) } })
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "レシート印字")

            val printReceipt by preferences.creditEnablePrint.collectAsState()
            Switch(checked = printReceipt, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setCreditEnablePrint(value = it) } })
        }
    }
}

@Composable
private fun SuicaInfo(preferences: UserPreferences) {
    Column {
        Row(horizontalArrangement = Arrangement.Start, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)) {
            val painter = painterResource(id = R.drawable.trans_ic)
            Image(painter = painter,
                contentDescription = "",
                modifier = Modifier.size(32.dp),
                contentScale = ContentScale.Fit)
            Text(text = "交通系IC", fontWeight = FontWeight.Bold, modifier = Modifier.padding(start = 10.dp))
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "有効")

            val enabled by preferences.suicaEnable.collectAsState()
            Switch(checked = enabled, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setSuicaEnable(value = it) } })
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "レシート印字")

            val printReceipt by preferences.suicaEnablePrint.collectAsState()
            Switch(checked = printReceipt, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setSuicaEnablePrint(value = it) } })
        }
    }
}

@Composable
private fun QuicPayInfo(preferences: UserPreferences) {
    Column {
        Row(horizontalArrangement = Arrangement.Start, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)) {
            val painter = painterResource(id = R.drawable.qp_card)
            Image(painter = painter,
                contentDescription = "",
                modifier = Modifier.size(32.dp),
                contentScale = ContentScale.Fit)
            Text(text = "QUICPay", fontWeight = FontWeight.Bold, modifier = Modifier.padding(start = 10.dp))
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "有効")

            val enabled by preferences.qpEnable.collectAsState()
            Switch(checked = enabled, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setQPEnable(value = it) } })
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "レシート印字")

            val printReceipt  by preferences.qpEnablePrint.collectAsState()
            Switch(checked = printReceipt, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setQPEnablePrint(value = it) } })
        }
    }
}

@Composable
private fun IDInfo(preferences: UserPreferences) {
    Column {
        Row(horizontalArrangement = Arrangement.Start, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)) {
            val painter = painterResource(id = R.drawable.id)
            Image(painter = painter,
                contentDescription = "",
                modifier = Modifier.size(32.dp),
                contentScale = ContentScale.Fit)
            Text(text = "iD", fontWeight = FontWeight.Bold, modifier = Modifier.padding(start = 10.dp))
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "有効")

            val enabled by preferences.idCreditEnable.collectAsState()
            Switch(checked = enabled, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setiDEnable(value = it) } })
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "税・その他")

            val duty by preferences.idCreditEnableDuty.collectAsState()
            Switch(checked = duty, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setiDEnableDuty(value = it) } })
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "商品コード")

            val prdCode by preferences.idCreditEnableProductCode.collectAsState()
            Switch(checked = prdCode, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setiDEnableProductCode(value = it) } })
        }
        Divider(modifier = Modifier.padding(start = 40.dp))
        Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
            .fillMaxWidth()
            .padding(start = 40.dp, end = 10.dp)) {
            Text(text = "レシート印字")

            val printReceipt by preferences.idCreditEnablePrint.collectAsState()
            Switch(checked = printReceipt, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setiDEnablePrint(value = it) } })
        }
    }
}

@Composable
private fun TrainingMode(preferences: UserPreferences) {
    Row(horizontalArrangement = Arrangement.SpaceBetween, verticalAlignment = Alignment.CenterVertically, modifier = Modifier
        .fillMaxWidth()
        .padding(10.dp)) {
        Text(text = "トレーニング", fontWeight = FontWeight.Bold)

        val training by preferences.trainingMode.collectAsState()
        Switch(checked = training, onCheckedChange = { CoroutineScope(Dispatchers.IO).launch { preferences.setTrainingMode(value = it) } })
    }
}
