package com.clementec.android.connect.api

import com.clementec.android.connect.data.json.LoginResponse
import kotlinx.coroutines.flow.Flow
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.security.MessageDigest

interface LoginService {
    @Headers("Content-Type: application/json", "Accept: application/json", "Cache-Control: no-cache")
    @POST("login/")
    suspend fun login(@Body body: RequestBody) : LoginResponse

    @Headers("Content-Type: application/json", "Accept: application/json", "Cache-Control: no-cache")
    @POST("login/")
    suspend fun login(@Body data: LoginRequestData) : LoginResponse
}

data class LoginRequestData(
    val id: String,
    val pass_word: String
)