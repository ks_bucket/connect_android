package com.clementec.android.connect

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.clementec.android.connect.data.menu.*
import androidx.lifecycle.viewmodel.compose.viewModel
import com.clementec.android.connect.data.json.LoginResponse

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Menu(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xfff0f0f0))
            .padding(start = 10.dp, end = 10.dp)
    ) {
        Scaffold(
            modifier = Modifier.weight(4f),
            topBar = { MenuBar(navController = navController, viewModel = viewModel) },
            backgroundColor = Color.Transparent
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 0.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                Products(loginResponse = loginResponse, viewModel = viewModel)
            }
        }

        Spacer(modifier = Modifier.width(10.dp))

        Scaffold(
            Modifier.weight(1f),
            topBar = { TopAppBar(title = { Text("ご注文リスト") }, modifier = Modifier.padding(0.dp).height(40.dp), backgroundColor = Color.Transparent, elevation = 0.dp) },
            backgroundColor = Color.Transparent
        ) {
            Payment(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }
    }
}

@Composable
fun MenuBar(navController: NavHostController, viewModel: ProductViewModel) {
    TopAppBar(
        title = { Text("") },
        modifier = Modifier.padding(0.dp).height(40.dp),
        navigationIcon = {
            var expanded by rememberSaveable { mutableStateOf(false) }
            Box(modifier = Modifier
                .fillMaxSize()
                .wrapContentSize(Alignment.TopStart)) {
                IconButton(modifier = Modifier.padding(start = 0.dp, top = 10.dp), onClick = { expanded = true }) {
                    Icon(Icons.Default.Menu, contentDescription = "Transition list")
                }
                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    DropdownMenuItem(onClick = {
                        expanded = false
                        navController.navigate(Screen.Setting.name)
                    }) {
                        Text(Screen.Setting.desc)
                    }
                    Divider()
                    DropdownMenuItem(onClick = {
                        expanded = false
                        navController.navigate(Screen.PaymentHistory.name)
                    }) {
                        Text(Screen.PaymentHistory.desc)
                    }
                    Divider()
                    DropdownMenuItem(onClick = {
                        expanded = false
                        navController.navigate(Screen.SuicaBalance.name)
                    }) {
                        Text(Screen.SuicaBalance.desc)
                    }
                    Divider()
                    DropdownMenuItem(onClick = {
                        viewModel.orderHandler.reset()
                        expanded = false
                        navController.navigate(Screen.Logout.name) { popUpTo(Screen.Menu.name) }
                    }) {
                        Text(Screen.Logout.desc)
                    }
                }
            }
        },
        backgroundColor = Color.Transparent,
        elevation = 0.dp
    )
}