package com.clementec.android.connect

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.clementec.android.connect.data.menu.*
import androidx.lifecycle.viewmodel.compose.viewModel
import com.clementec.android.connect.data.json.LoginResponse

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun ReceiptSelection(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(modifier = Modifier
            .fillMaxWidth()
            .background(Color(0xfff0f0f0))
            .padding(start = 10.dp, end = 10.dp)
    ) {
        Scaffold(
            modifier = Modifier.weight(4f),
            topBar = { TopBar(navController = navController, title = "レシート受け取り方法を選択してください") },
            backgroundColor = Color.Transparent
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 10.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                Receipts(navController = navController)
            }
        }

        Spacer(modifier = Modifier.width(10.dp))

        Scaffold(
            Modifier.weight(1f),
            topBar = { TopAppBar(title = { Text("ご注文リスト") }, modifier = Modifier.padding(0.dp).height(40.dp), backgroundColor = Color.Transparent, elevation = 0.dp) },
            backgroundColor = Color.Transparent
        ) {
            PaymentInfo(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }
    }
}

@Composable
fun Receipts(navController: NavHostController) {
    Row(modifier = Modifier.fillMaxSize(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically) {
        Column(modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Row(horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically) {
                PaperReceipt(navController = navController)
                Spacer(modifier = Modifier.width(100.dp))
                DigitalReceipt(navController = navController)
            }
            
            Spacer(modifier = Modifier.height(20.dp))
            NoReceipt(navController = navController)
        }
    }
}

@Composable
fun PaperReceipt(navController: NavHostController) {
    Column(modifier = Modifier.clickable { navController.navigate(Screen.PaymentCategory.name) },
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(80.dp)) {
            val painter = painterResource(id = R.drawable.paper_receipt)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(20.dp))

        Text(text = "紙レシート", fontSize = 18.sp)
    }
}

@Composable
fun DigitalReceipt(navController: NavHostController) {
    Column(modifier = Modifier.clickable { navController.navigate(Screen.PaymentCategory.name) },
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(80.dp)) {
            val painter = painterResource(id = R.drawable.digital_receipt)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(20.dp))

        Text(text = "デジタルレシート", fontSize = 18.sp)
    }
}

@Composable
fun NoReceipt(navController: NavHostController) {
    Surface(
        modifier = Modifier
            .width(300.dp)
            .height(80.dp)
            .padding(10.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        OutlinedButton(
            onClick = { navController.navigate(Screen.PaymentCategory.name) },
            modifier = Modifier.fillMaxSize(),
            colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color(0xFF797979), contentColor = Color.White, disabledContentColor = Color(0xFF797979)),
        ) {
            Text("レシートは受け取らない", fontSize = 18.sp, color = Color.White, maxLines = 1, overflow = TextOverflow.Ellipsis)
        }
    }
}