package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class Screen(
    @Json(name = "layout_cd") val layoutCd: Int,
    @Json(name = "screen_name") val screenName: String
)