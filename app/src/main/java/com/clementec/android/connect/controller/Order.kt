package com.clementec.android.connect.controller

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.clementec.android.connect.Util.toLocalDatetime
import com.clementec.android.connect.data.persistence.Product
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

data class Order(val product: Product, val date: Date = Date()) {
    var count by mutableStateOf<Int>( 1 )
}
