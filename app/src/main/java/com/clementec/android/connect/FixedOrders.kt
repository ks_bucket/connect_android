package com.clementec.android.connect

import android.inputmethodservice.Keyboard
import androidx.compose.animation.*
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.outlined.ExpandLess
import androidx.compose.material.icons.outlined.ExpandMore
import androidx.compose.material.icons.twotone.Cancel
import androidx.compose.material.icons.twotone.HighlightOff
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.clementec.android.connect.controller.Order
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.model.ProductViewModel
import com.clementec.android.connect.data.persistence.Product

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun FixedOrders(loginResponse: LoginResponse, viewModel: ProductViewModel) {
    val orders by viewModel.orders.collectAsState()
    val orderHandler = viewModel.orderHandler

    LazyColumn(modifier = Modifier.fillMaxSize().padding(10.dp)) {
        items(orders) {
            FixedOrder(it, orderHandler)
        }
    }
}

@Composable
private fun FixedOrder(order: Order, orderHandler: OrderHandler) {
    Surface(
        modifier = Modifier.padding(bottom = 8.dp),
        elevation = 2.dp,
        shape = RoundedCornerShape(8.dp)
    ) {
        Box {
            Row(modifier = Modifier.fillMaxWidth().background(Color.White)) {
                Image(bitmap = order.product.image!!.asImageBitmap(),
                    contentDescription = "Map snapshot",
                    modifier = Modifier.weight(2f).height(60.dp),
                    contentScale = ContentScale.Fit
                )

                Column(modifier = Modifier.weight(4f)) {
                    Text(text = order.product.name, fontSize = 12.sp, modifier = Modifier.padding(8.dp), maxLines = 1, color = Color.Black)
                    Text(text = "¥ " + order.product.price.toString(), fontSize = 12.sp, modifier = Modifier.padding(8.dp), color = Color.Black)
                }

                FixedOrderCount(order = order, orderHandler = orderHandler)
            }
        }
    }
}

@Composable
private fun FixedOrderCount(order: Order, orderHandler: OrderHandler) {
    val count = order.count
    Surface(
        modifier = Modifier.padding(2.dp),
        shape = RoundedCornerShape(4.dp)
    ) {
        Column(modifier = Modifier.height(60.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = count.toString(),
                color = Color.Black,
                fontSize = 16.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.width(20.dp))
        }
    }
}