package com.clementec.android.connect

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.material.icons.filled.CheckCircle
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.model.ProductViewModel
import com.clementec.android.connect.data.persistence.Product
import androidx.lifecycle.viewmodel.compose.viewModel
import com.clementec.android.connect.controller.Order
import com.clementec.android.connect.controller.OrderHandler
import com.clementec.android.connect.data.persistence.Sale
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter

@ExperimentalFoundationApi
@Composable
fun Products(loginResponse: LoginResponse, viewModel: ProductViewModel) {
    val products by viewModel.products(loginResponse = loginResponse).collectAsState()
    val orders by viewModel.orders.collectAsState()
    val orderHandler = viewModel.orderHandler

    Column() {
        var category = rememberSaveable { mutableStateOf("") }
        LazyRow(contentPadding = PaddingValues(8.dp),
            reverseLayout = true,
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.Transparent),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start) {
            items(products.map { it.categoryName }.filterNotNull().distinct(). filter { it != "" }) { name ->
                Category(name = name, selected = category.value, onClick = { category.value = name })
            }
            items(listOf("全商品")) { name ->
                Category(name = name, selected = category.value, onClick = { category.value = name })
            }
        }

        LazyVerticalGrid(
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 4.dp, top = 0.dp, end = 4.dp),
            cells = GridCells.Fixed(5),
            contentPadding = PaddingValues(0.dp)
        ) {
            items(products.filter { it.display }.filter { if (category.value == "" || category.value == "全商品") true else category.value == it.categoryName }) { product ->
                Product(product, orders, select = { orderHandler.add(product)})
            }
        }
    }
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
private fun Product(product: Product, orders: List<Order>, select: () -> Unit = {}) {
    var state = remember {
        MutableTransitionState(false).apply {
            // Start the animation immediately.
            targetState = true
        }
    }

    AnimatedVisibility(visibleState = state, enter = fadeIn(), exit = fadeOut()) {
        Surface(
            modifier = Modifier
                .padding(4.dp)
                .clickable(
                    onClick = { select.invoke() }
                ),
            elevation = 2.dp,
            shape = RoundedCornerShape(8.dp)
        ) {
            Box(modifier = Modifier.fillMaxSize()) {
                Column(modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White)
                ) {
                    Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
                        Image(bitmap = product.image!!.asImageBitmap(),
                            contentDescription = "Map snapshot",
                            modifier = Modifier
                                .height(120.dp)
                                .aspectRatio(1.4f),
                            contentScale = ContentScale.Fit,
                            alignment = Alignment.Center
                        )
                    }

                    Text(text = product.name, maxLines = 1,modifier = Modifier.padding(top = 16.dp, start = 16.dp, end = 16.dp), color = Color.Black)
                    Text(text = "¥ " + product.price.toString(), modifier = Modifier.padding(16.dp), color = Color.Black)
                }

                if (orders.find { it.product.cd == product.cd } != null) {
                    Row(modifier = Modifier
                        .fillMaxSize()
                        .padding(10.dp), horizontalArrangement = Arrangement.End) {
                        Icon(Icons.Filled.CheckCircle,
                            contentDescription = "Transition list",
                            modifier = Modifier.size(32.dp))
                    }
                }
            }
        }
    }
}