package com.clementec.android.connect.api

import android.content.Context
import coil.ImageLoader
import coil.imageLoader
import com.clementec.android.connect.ConnectApplication
import com.clementec.android.connect.di.DiModule
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.net.CookieHandler
import java.net.CookieManager
import javax.inject.Inject

object Api{
//    private val context = ConnectApplication.instance
//    const val baseURL = "https://stg-connect.paymentcl.com"
//    private const val apiPath = "/api/"
//    private val cookieHandler: CookieHandler = CookieManager()
//    private val interceptor: HttpLoggingInterceptor by lazy {
//        HttpLoggingInterceptor()
//            .setLevel(HttpLoggingInterceptor.Level.BODY)
//    }
//
//    private val client: OkHttpClient by lazy {
//        OkHttpClient
//            .Builder()
//            .addInterceptor(interceptor)
//            .cookieJar(JavaNetCookieJar(cookieHandler))
//            .build()
//    }
//
//    private val moshi: Moshi by lazy {
//        Moshi.Builder()
//            .add(KotlinJsonAdapterFactory())
//            .build()
//    }
//    private val retrofit: Retrofit by lazy {
//        Retrofit.Builder()
//            .client(client)
//            .addConverterFactory(MoshiConverterFactory.create(moshi))
//            .baseUrl(baseURL + apiPath)
//            .build()
//    }
//
//    val login: LoginService by lazy { retrofit.create(LoginService::class.java) }
//    val remoteData: ProductDataService by lazy { retrofit.create(ProductDataService::class.java) }
//    val loader: Loader by lazy { Loader(context, client) }
}