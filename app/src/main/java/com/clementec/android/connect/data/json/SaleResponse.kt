package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class SaleResponse(
    // 結果
    val  result: Int,
    // エラー№
    @Json(name = "err_no") val errNo: String,
    // エラーメッセージ
    @Json(name = "err_msg") val errMsg: String,
    // レシートURL
    @Json(name = "receipt_url") val receiptUrl: String
)
