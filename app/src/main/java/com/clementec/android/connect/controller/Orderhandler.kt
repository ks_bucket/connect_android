package com.clementec.android.connect.controller

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.clementec.android.connect.api.ApiState
import com.clementec.android.connect.data.persistence.Product
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

class OrderHandler @Inject constructor() {
    private val mutableList: MutableList<Order> = mutableListOf<Order>()
    private val _list: MutableStateFlow<List<Order>> = MutableStateFlow( emptyList() )
    val list: StateFlow<List<Order>> = _list
    var price by mutableStateOf<Int>( 0 )
    var tax by mutableStateOf<Int>( 0 )

    fun add(product: Product) {
        mutableList.find { it.product.cd == product.cd }?.let {
            it.count += 1
        } ?: mutableList.add(0, Order(product = product))

        price =  mutableList.sumOf { it.product.price * it.count }
        tax = mutableList.sumOf { it.product.price * it.count * it.product.taxRate } / 100
        _list.value = mutableList.toList()
        Log.d("Order:", mutableList.count().toString())
    }

    fun decrease(product: Product) {
        mutableList.find { it.product.cd == product.cd }?.also { it.count -= 1 }
        mutableList.removeAll { it.count < 1 }
        price =  mutableList.sumOf { it.product.price * it.count }
        tax = mutableList.sumOf { it.product.price * it.count * it.product.taxRate } / 100
        _list.value = mutableList.toList()
        Log.d("Order:", mutableList.count().toString())
    }

    fun delete(product: Product) {
        mutableList.find { it.product.cd == product.cd }?.let {
            mutableList.remove(it)
        }

        price =  mutableList.sumOf { it.product.price * it.count }
        tax = mutableList.sumOf { it.product.price * it.count * it.product.taxRate } / 100
        _list.value = mutableList.toList()
        Log.d("Order:", mutableList.count().toString())
    }
    
    fun reset() {
        mutableList.clear()

        price =  mutableList.sumOf { it.product.price * it.count }
        tax = mutableList.sumOf { it.product.price * it.count * it.product.taxRate } / 100
        _list.value = mutableList.toList()
        Log.d("Order:", mutableList.count().toString())
    }

    fun included(product: Product): Boolean {
        return mutableList.find { it.product.cd == product.cd } != null
    }
}