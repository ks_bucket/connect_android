package com.clementec.android.connect.api

import com.clementec.android.connect.data.json.ProductDataSource
import com.clementec.android.connect.data.json.SaleResponse
import com.squareup.moshi.JsonClass
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface SaleService {
    @Headers("Content-Type: application/json", "Accept: application/json", "Cache-Control: no-cache")
    @POST("payment/")
    suspend fun sync(@Body data: SaleData) : SaleResponse
}

@JsonClass(generateAdapter = true)
data class SaleData(
    // ログインセッションID
    val session_id: String,
    // 利用サービスID
    val use_service_id: Int,
    // 端末アカウント
    val user_id: Int,
    // QRコード電文
    val qr_cd: String,
    // 売上情報
    val order_info: List<SaleOrderInfo>
)

@JsonClass(generateAdapter = true)
data class SaleOrderInfo(
    // 端末UDID
    val terminal_udid: String,
    // 登録日時
    val order_date: String,
    // 決済種別
    val order_host_type: String,
    // 伝票番号
    val slip_number: String,
    // 取引分類 1:売上/2:取消
    val record_type: Int,
    // 合計金額
    val total_price: Int,
    // 消費税額
    val consumption_tax: Int,
    // 取引詳細（配列）
    val order_detail: List<OrderDetail>
)

@JsonClass(generateAdapter = true)
data class OrderDetail(
    // 商品コード
    val product_cd: String,
    // 商品名
    val product_name: String,
    // 個数
    val product_quantity: Int,
    // 単価
    val product_price: Int,
    // 消費税区分
    val product_tax_type: Int,
    // 税率
    val product_tax_rate: Int
)

@JsonClass(generateAdapter = true)
data class Record(val type: Record.Type) {
    val value
    get()  = run {
        when (type) {
            Type.SALE -> 1
            Type.CANCEL -> 2
            Type.RECEIPT -> 3
        }
    }

    enum class Type {
        SALE,
        CANCEL,
        RECEIPT
    }
}



data class Service(val type: Service.Type) {
    val value
        get()  = run {
            when (type) {
                Type.Suica -> "TR"
                Type.QP -> "QU"
                Type.ID -> "ID"
                Type.Credit -> "CR"
                Type.Kyuyo -> "SA"
            }
        }

    enum class Type {
        Suica,
        QP,
        ID,
        Credit,
        Kyuyo
    }
}

