package com.clementec.android.connect.data.repository

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.clementec.android.connect.api.Api
import com.clementec.android.connect.api.ApiState
import com.clementec.android.connect.api.LoginRequestData
import com.clementec.android.connect.api.LoginService
import com.clementec.android.connect.data.json.LoginResponse
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.security.MessageDigest
import javax.inject.Inject
import javax.inject.Singleton

class LoginRepository @Inject constructor(private val api: LoginService) {
    private val scope = CoroutineScope(Dispatchers.IO)
    private val salt = "s7_il#EM01TL&a2!"

    private fun digest(code: String): String {
        return MessageDigest.getInstance("SHA-256")
            .digest(code.toByteArray())
            .joinToString(separator = "") {
                "%02x".format(it)
            }
    }

    private fun requestBody(id: String, pwd: String): RequestBody {
        val encoded = pwd + salt
        val pass = digest(encoded)
        val jsonObject = JSONObject()
        jsonObject.put("id", id)
        jsonObject.put("pass_word", pass)

        val jsonObjectString = jsonObject.toString()
        return jsonObjectString.toRequestBody("application/json".toMediaTypeOrNull())
    }

    private fun request(id: String, pwd: String): LoginRequestData {
        val encoded = pwd + salt
        val pass = digest(encoded)
        return LoginRequestData(id, pass)
    }

    suspend fun login(id: String, pwd: String): LoginResponse? = coroutineScope {
        val data = request(id, pwd)
        val deferred = async(Dispatchers.IO) {
            try {
                api.login(data)
            } catch(ex: Throwable) {
                Log.d("Login", "$ex")
                null
            }
        }

        deferred.await()
    }
}