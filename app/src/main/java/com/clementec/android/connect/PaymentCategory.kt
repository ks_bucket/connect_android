package com.clementec.android.connect

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.clementec.android.connect.data.menu.*
import androidx.lifecycle.viewmodel.compose.viewModel
import com.clementec.android.connect.api.Record
import com.clementec.android.connect.api.Service
import com.clementec.android.connect.data.json.LoginResponse
import java.util.*

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun PaymentCategory(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .background(Color(0xfff0f0f0))
        .padding(start = 10.dp, end = 10.dp)
    ) {
        Scaffold(
            modifier = Modifier.weight(4f),
            topBar = { TopBar(navController = navController, title = "お支払い方法を選択してください") },
            backgroundColor = Color.Transparent
        ) {
            Surface(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(bottom = 10.dp),
                elevation = 2.dp,
                shape = RoundedCornerShape(8.dp)
            ) {
                CategoryList(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
            }
        }

        Spacer(modifier = Modifier.width(10.dp))

        Scaffold(
            Modifier.weight(1f),
            topBar = { TopAppBar(title = { Text("ご注文リスト") }, modifier = Modifier.padding(0.dp).height(40.dp), backgroundColor = Color.Transparent, elevation = 0.dp) },
            backgroundColor = Color.Transparent
        ) {
            PaymentInfo(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }
    }
}

@Composable
fun CategoryList(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Row(modifier = Modifier.fillMaxSize(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically) {
        Column(modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center) {
            Row(horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically) {
                Creditcard(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
                Spacer(modifier = Modifier.width(50.dp))
                Emoney(navController = navController)
                Spacer(modifier = Modifier.width(50.dp))
                Kyuyo(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
            }
        }
    }
}

@Composable
fun Creditcard(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Column(modifier = Modifier.clickable {
        viewModel.order(loginResponse = loginResponse,
            qr = "",
            hostType = Service(type = Service.Type.Credit).value,
            slipNumber = UUID.randomUUID().toString(),
            recordType = Record(type = Record.Type.SALE),
            payment = "")
    }, horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(150.dp)) {
            val painter = painterResource(id = R.drawable.credit_card)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(32.dp))

        Text(text = "クレジットカード", fontSize = 18.sp)
    }
}

@Composable
fun Emoney(navController: NavHostController) {
    Column(modifier = Modifier.clickable { navController.navigate(Screen.Emoney.name) },
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(150.dp)) {
            val painter = painterResource(id = R.drawable.emoney)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(32.dp))

        Text(text = "電子マネー", fontSize = 18.sp)
    }
}

@Composable
fun Kyuyo(navController: NavHostController, loginResponse: LoginResponse, viewModel: ProductViewModel) {
    Column(modifier = Modifier.clickable { },
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center) {
        Column(modifier = Modifier.width(150.dp)) {
            val painter = painterResource(id = R.drawable.qr)
            Image(painter = painter,
                contentDescription = "",
                contentScale = ContentScale.Fit)
        }

        Spacer(modifier = Modifier.height(32.dp))

        Text(text = "給与天引き", fontSize = 18.sp)
    }
}
