package com.clementec.android.connect

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.clementec.android.connect.data.menu.Screen
import com.clementec.android.connect.data.model.LoginViewModel
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.clementec.android.connect.api.Api
import com.clementec.android.connect.data.database.Database
import com.clementec.android.connect.data.json.LoginResponse
import com.clementec.android.connect.data.repository.ProductRepository
import com.clementec.android.connect.di.DiModule

@OptIn(ExperimentalFoundationApi::class, androidx.compose.material.ExperimentalMaterialApi::class)
@Composable
fun NavGraph(loginResponse: LoginResponse, login: LoginViewModel, viewModel: ProductViewModel) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Screen.Menu.name) {
        composable(route = Screen.Menu.name) {
            Menu(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }

        composable(route = Screen.Receipt.name) {
            ReceiptSelection(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }

        composable(route = Screen.PaymentCategory.name) {
            PaymentCategory(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }

        composable(route = Screen.Emoney.name) {
            EmoneySelection(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }

        composable(route = Screen.Setting.name) {
            Setting(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }

        composable(route = Screen.PaymentHistory.name) {
            PaymentHistory(navController = navController, loginResponse = loginResponse, viewModel = viewModel)
        }

        composable(route = Screen.Logout.name) {
            Logout(viewModel = login)
        }
    }
}