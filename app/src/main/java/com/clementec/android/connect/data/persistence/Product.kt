package com.clementec.android.connect.data.persistence

import android.graphics.Bitmap
import androidx.annotation.NonNull
import androidx.room.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged

@Dao
interface ProductDao {
    @Query("SELECT * FROM product WHERE service_id == :serviceId AND company_id == :companyId ORDER BY cd ASC")
    fun getAll(serviceId: Int, companyId: Int): Flow<List<Product>>

    @Query("SELECT * FROM product WHERE cd IN (:cds) AND service_id == :serviceId AND company_id == :companyId ORDER BY cd ASC")
    fun loadAllByCds(cds: List<String>, serviceId: Int, companyId: Int): Flow<List<Product>>

    @Query("SELECT * FROM product WHERE cd == :cd AND service_id == :serviceId AND company_id == :companyId LIMIT 1")
    fun loadAllByCd(cd: String, serviceId: Int, companyId: Int): Flow<Product>

    @Query("SELECT * FROM product WHERE name == :name AND service_id == :serviceId AND company_id == :companyId LIMIT 1")
    fun findByName(name: String, serviceId: Int, companyId: Int): Flow<Product>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(product: Product)

    @Update
    suspend fun update(product: Product)

    @Delete
    suspend fun delete(product: Product)

    @Query("SELECT id, cd, name, price, display FROM product WHERE service_id == :serviceId AND company_id == :companyId ORDER BY cd ASC")
    fun productMinimals(serviceId: Int, companyId: Int): Flow<List<ProductMinimal>>

    fun distinctProduct(cd: String, serviceId: Int, companyId: Int) = loadAllByCd(cd, serviceId, companyId).distinctUntilChanged()
}

@Entity
data class Product (
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @NonNull val cd: String,                                            // 商品コード
    @NonNull var name: String,                                          // 商品名
    @NonNull var price: Int,                                            // 単価
    @NonNull @ColumnInfo(name = "tax_type") val taxType: Int,           // 消費税区分
    @NonNull @ColumnInfo(name = "tax_rate") val taxRate: Int,           // 税率
    @ColumnInfo(name = "category_name") val categoryName: String?,      // カテゴリ名
    @ColumnInfo(name = "image_file_url") val imageFileUrl: String?,     // 商品画像URL
    @ColumnInfo(name = "image_updated_at") val imageUpdatedAt: String?, // 画像更新日時
    @NonNull @ColumnInfo(name = "sort_no") val sortNo: Int,             // ソート順
    @ColumnInfo(name = "custom_data") val customData: String?,          // 商品カスタムデータ
    @NonNull @ColumnInfo(name = "service_id") val serviceId: Int,
    @NonNull @ColumnInfo(name = "company_id") val companyId: Int,
    @NonNull val date: String,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) val image: Bitmap?,
    @NonNull var display: Boolean = true
)

data class ProductMinimal (
    val id: Int,
    val cd: String,
    var name: String,
    var price: Int,
    var display: Boolean = true
)