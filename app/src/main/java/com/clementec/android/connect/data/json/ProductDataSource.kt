package com.clementec.android.connect.data.json

import com.squareup.moshi.Json

data class ProductDataSource(
    @Json(name = "err_msg") val errMsg: String,
    @Json(name = "err_no") val errNo: String,
    val product: List<ProductData>,
    val result: Int,
    val screen: Screen,
    val title: Title
)