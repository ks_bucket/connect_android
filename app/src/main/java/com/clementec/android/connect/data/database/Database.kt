package com.clementec.android.connect.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.clementec.android.connect.ConnectApplication
import com.clementec.android.connect.data.persistence.Product
import com.clementec.android.connect.data.persistence.ProductDao
import com.clementec.android.connect.data.persistence.Sale
import com.clementec.android.connect.data.persistence.SaleDao

@Database(entities = [Product::class, Sale::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class Database: RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun saleDao(): SaleDao
}