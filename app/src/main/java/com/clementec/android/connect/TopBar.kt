package com.clementec.android.connect

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import kotlinx.coroutines.launch
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.clementec.android.connect.data.model.ProductViewModel
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.text.style.TextAlign
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.clementec.android.connect.data.menu.*

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun TopBar(navController: NavHostController, title: String) {
    TopAppBar(
        title = { Text(title, textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth()) },
        modifier = Modifier.padding(0.dp).height(40.dp),
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() },
                modifier = Modifier.padding(0.dp)) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "Localized description")
            }
        },
        backgroundColor = Color.Transparent,
        elevation = 0.dp
    )
}
