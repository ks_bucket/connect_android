package com.clementec.android.connect.api

import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableStateFlow

enum class ApiState {
    Idle,
    Loading,
    Error,
    Done
}