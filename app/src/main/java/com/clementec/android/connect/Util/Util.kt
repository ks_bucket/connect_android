package com.clementec.android.connect.Util

import java.sql.Timestamp
import java.text.DateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

private const val pattern = "yyyy-MM-dd HH:mm:ss"

fun String.toLocalDateTime(format: String = pattern): LocalDateTime = LocalDateTime.parse(this, DateTimeFormatter.ofPattern(format))
fun LocalDateTime.toString(pat: String = pattern): String = pat.format(this)
fun Date.toLocalDatetime(): LocalDateTime = this.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()
fun LocalDateTime.toDate(): Date = Date.from(this.atZone(ZoneId.systemDefault()).toInstant())