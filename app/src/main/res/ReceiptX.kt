data class ReceiptX(
    val address: String,
    val branch_name: String,
    val postal_code: String,
    val remarks: String
)